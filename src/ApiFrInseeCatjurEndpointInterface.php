<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur;

use Iterator;
use Stringable;

/**
 * ApiFrInseeCatjurEndpointInterface interface file.
 * 
 * This interface represents an unique endpoint to get juridic category data.
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCatjurEndpointInterface extends Stringable
{
	
	/**
	 * Gets an iterator over all the juridic categories lv1.
	 *
	 * @return Iterator<integer, ApiFrInseeCatjurCategoryLv1Interface>
	 */
	public function getJuridicCategoryLv1Iterator() : Iterator;
	
	/**
	 * Gets an iterator over all the juridic categories lv2.
	 *
	 * @return Iterator<integer, ApiFrInseeCatjurCategoryLv2Interface>
	 */
	public function getJuridicCategoryLv2Iterator() : Iterator;
	
	/**
	 * Gets an iterator over all the juridic categories lv3.
	 * 
	 * @return Iterator<integer, ApiFrInseeCatjurCategoryLv3Interface>
	 */
	public function getJuridicCategoryLv3Iterator() : Iterator;
	
}

<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur;

use Stringable;

/**
 * ApiFrInseeCatjurCategoryLv3Interface interface file.
 * 
 * This is the lv3 of the juridic categories.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCatjurCategoryLv3Interface extends Stringable
{
	
	/**
	 * Gets the id of the category.
	 * 
	 * @return int
	 */
	public function getIdLv3() : int;
	
	/**
	 * Gets the id of the related lv2.
	 * 
	 * @return int
	 */
	public function getIdLv2() : int;
	
	/**
	 * Gets the year when this category was created.
	 * 
	 * @return int
	 */
	public function getCreatedYear() : int;
	
	/**
	 * Gets the year when this category was removed.
	 * 
	 * @return ?int
	 */
	public function getRemovedYear() : ?int;
	
	/**
	 * Gets the label of the category.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}

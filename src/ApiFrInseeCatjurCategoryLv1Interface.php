<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-insee-catjur-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrInseeCatjur;

use Stringable;

/**
 * ApiFrInseeCatjurCategoryLv1Interface interface file.
 * 
 * This is the lv1 of the juridic categories.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrInseeCatjurCategoryLv1Interface extends Stringable
{
	
	/**
	 * Gets the id of the category.
	 * 
	 * @return int
	 */
	public function getIdLv1() : int;
	
	/**
	 * Gets the label of the category.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
}
